// 1.
public class ArrayManipulation {
    public static void main(String[] args) throws Exception {
        // 2.
        int[] numbers = {5,8,3,2,7} ;
        String[] names = {"Alice","Bob","Charlie","David"};
        double[] values = new double[4] ;
        int sum = 0 ;
        double max = 0 ;
        int findMax = 0 ;
        // 3.
        for (int i = 0 ; i < numbers.length ; i++){System.out.print(numbers[i]+" ");}
        System.out.println();
        // 4.
        for (int i = 0 ; i < names.length ; i++){System.out.print(names[i]+" ");}
        System.out.println();
        // 5.        
        for (int i = 0 ; i < values.length ; i++){values[i] = i+1;}
        for (int i = 0 ; i < values.length ; i++){System.out.print(values[i]+" ");}
        System.out.println();
        // 6.
        for (int i = 0 ; i < numbers.length ; i++){sum += numbers[i];}
        System.out.println(sum);
        // 7.
        for (int i = 0 ; i < values.length ; i++){if (max < values[i]){max = values[i];}}
        System.out.println(max);
        // 8.
        String[] reversedNames = new String[names.length];
        for (int i = 0 ; i < reversedNames.length ; i++){reversedNames[i] = names[-i+(reversedNames.length-1)];}
        for (int i = 0 ; i < values.length ; i++){System.out.print(reversedNames[i]+" ");}
        System.out.println();
        // 9.
        int[] copyNumber = numbers ;
        int[] sortNumber = new int[numbers.length];
        int[] sortNumberMinToMax = new int[sortNumber.length];
        for (int i = 0 ; i < sortNumber.length ; i++){for(int j=0;j<copyNumber.length;j++)
            {if (findMax<copyNumber[j]){findMax=copyNumber[j];}}sortNumber[i]=findMax;
            for(int k=0;k<copyNumber.length;k++){if(copyNumber[k]==findMax){copyNumber[k]=0;}}findMax=0;}
        //for (int i = 0 ; i < sortNumber.length ; i++){System.out.print(sortNumber[i]+" ");}
        for (int i = 0 ; i < sortNumber.length ; i++){sortNumberMinToMax[i] = sortNumber[-i+(sortNumber.length-1)];}
        for (int i = 0 ; i < sortNumberMinToMax.length ; i++){System.out.print(sortNumberMinToMax[i]+" ");}
        
    }
}