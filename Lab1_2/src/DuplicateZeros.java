public class DuplicateZeros {
    public static void main(String[] args) {
        int[] arr = {1,0,2,3,0,4,5,0};
        int[] newArr = new int[arr.length];
        
        for (int i = 0, j = 0; i < arr.length && j < newArr.length; i++, j++) {
            newArr[j] = arr[i];
            if (arr[i] == 0 && j < newArr.length - 1) {
                j++;
                newArr[j] = arr[i];
            }
        }

        for (int i = 0; i < newArr.length; i++) {
            System.out.print(newArr[i] + " ");
        }
    }
}
